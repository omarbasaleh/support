import XCTest

import NabdSupportTests

var tests = [XCTestCaseEntry]()
tests += NabdSupportTests.allTests()
XCTMain(tests)
