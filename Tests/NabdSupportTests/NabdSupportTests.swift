import XCTest
@testable import NabdSupport

final class NabdSupportTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(NabdSupport().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
