//
//  Array+remove.swift
//  Nabd_Support
//
//  Created by Omar Basaleh on 7/16/20.
//  Copyright © 2020 Omar Basaleh. All rights reserved.
//

import Foundation

public extension Array {
    mutating func remove(at indexes: [Int]) {
        var lastIndex: Int? = nil
        for index in indexes.sorted(by: >) {
            guard lastIndex != index else {
                continue
            }
            remove(at: index)
            lastIndex = index
        }
    }
}
