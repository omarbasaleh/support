//
//  String+booleanValue.swift
//  Nabd_Support
//
//  Created by Omar Basaleh on 7/16/20.
//  Copyright © 2020 Omar Basaleh. All rights reserved.
//

import Foundation

public extension String {
    var booleanValue: Bool {
        if self == "1"{
            return true
        }else{
            return false
        }
    }
}
