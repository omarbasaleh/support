//
//  Strin+localized.swift
//  Code Migration
//
//  Created by Omar Basaleh on 10/29/19.
//  Copyright © 2019 Omar Basaleh. All rights reserved.
//

import Foundation

public extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
