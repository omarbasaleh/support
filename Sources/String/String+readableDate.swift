//
//  String+readableDate.swift
//  Nabd_Support
//
//  Created by Omar Basaleh on 7/16/20.
//  Copyright © 2020 Omar Basaleh. All rights reserved.
//

import Foundation

public extension String {
    
    func readableDate() -> String {
        let formatter = DateFormatter()
        
        let usLocale = Locale(identifier: "en_US")
        formatter.calendar = Calendar(identifier: .gregorian)
        
        formatter.formatterBehavior = .behavior10_4
        formatter.timeZone = TimeZone(identifier: "Asia/Kuwait")
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter.locale = usLocale
        
        NSTimeZone.default = TimeZone(identifier: "Asia/Kuwait")!
        let date = Date()
        
        let currentDateString = formatter.string(from: date)
        let dateToConvertString = self
        
        guard let currentDate = formatter.date(from: currentDateString) else {return ""}
        guard let dateToConvert = formatter.date(from: dateToConvertString) else {return ""}
        
        let timeDifference: TimeInterval = currentDate.timeIntervalSince(dateToConvert)
        
        // Calculating the hours
        var weeks = 0, days = 0, hours = 0, minutes = 0, remaining = 0
        
        if timeDifference / 60 <= 1 {
            return "Now".localized
        }
        
        hours = Int(timeDifference / (60*60))
        remaining = Int(timeDifference) % (60*60)
        
        if hours > 23{
            days = hours / 24
            
            if days > 6 {
                weeks = days / 7
            }
        }
        
        if remaining > 0 {
            minutes = remaining / 60
        }
        
        if weeks != 0 {
            if SwiftConstants.isRTL{
                switch weeks {
                case 1:
                    return "Since".localized + " " + "Week".localized
                case 2:
                    return "Since".localized + " " + "TwoWeeks".localized
                case 3...10:
                    return "Since".localized + " " + "\(weeks)" + " " + "Weeks".localized
                default:
                     return "Since".localized + " " + "\(weeks)" + " " + "Week".localized
                }
            }else{
                return "\(weeks) " + "Weeks".localized
            }
        }
        
        if days != 0 {
           if SwiftConstants.isRTL{
                switch days {
                case 1:
                    return "Since".localized + " " + "يوم"
                case 2:
                    return "Since".localized + " " + "يومين"
                case 3...10:
                    return "Since".localized + " " + "\(days)" + " " + "أيام"
                default:
                     return "Since".localized + " " + "\(days)" + " " + "يوم"
                }
            }else{
                return "\(days) " + "Days".localized
            }
        }
        
        if hours != 0 {
           if SwiftConstants.isRTL{
                switch hours {
                case 1:
                    return "Since".localized + " " + "Hour".localized
                case 2:
                    return "Since".localized + " " + "TwoHours".localized
                case 3...10:
                    return "Since".localized + " " + "\(hours)" + " " + "Hours".localized
                default:
                     return "Since".localized + " " + "\(hours)" + " " + "Hour".localized
                }
            }else{
                return "\(hours) " + "Hours".localized
            }
        }
        
        if minutes != 0 {
           if SwiftConstants.isRTL{
                switch minutes {
                case 1:
                    return "Since".localized + " " + "Minute".localized
                case 2:
                    return "Since".localized + " " + "TwoMinutes".localized
                case 3...10:
                    return "Since".localized + " " + "\(minutes)" + " " + "Minutes".localized
                default:
                     return "Since".localized + " " + "\(minutes)" + " " + "Minute".localized
                }
            }else{
                return "\(minutes) " + "Minutes".localized
            }
        }
        
        return ""
    }
}
