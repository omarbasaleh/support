//
//  UIView+addBorders.swift
//  Nabd_Support
//
//  Created by Omar Basaleh on 7/16/20.
//  Copyright © 2020 Omar Basaleh. All rights reserved.
//

import Foundation

extension UIView {
    
    func addBorders(edges: UIRectEdge,
                    color: UIColor,
                    inset: CGFloat = 0.0,
                    thickness: CGFloat = 1.0){


        if edges.contains(.top) || edges.contains(.all) {
            let border = UIView()
            border.backgroundColor = color
            border.translatesAutoresizingMaskIntoConstraints = false
            addSubview(border)
            border.heightAnchor.constraint(equalToConstant: thickness).isActive = true
            border.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
            border.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
            border.topAnchor.constraint(equalTo: topAnchor).isActive = true
        }

        if edges.contains(.bottom) || edges.contains(.all) {
            let border = UIView()
            border.backgroundColor = color
            border.translatesAutoresizingMaskIntoConstraints = false
            addSubview(border)
            border.heightAnchor.constraint(equalToConstant: thickness).isActive = true
            border.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
            border.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
            border.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        }

        if edges.contains(.left) || edges.contains(.all) {
            let border = UIView()
            border.backgroundColor = color
            border.translatesAutoresizingMaskIntoConstraints = false
            addSubview(border)
            border.widthAnchor.constraint(equalToConstant: thickness).isActive = true
            border.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
            border.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
            border.topAnchor.constraint(equalTo: topAnchor).isActive = true
        }

        if edges.contains(.right) || edges.contains(.all) {
            let border = UIView()
            border.backgroundColor = color
            border.translatesAutoresizingMaskIntoConstraints = false
            addSubview(border)
            border.widthAnchor.constraint(equalToConstant: thickness).isActive = true
            border.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
            border.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
            border.topAnchor.constraint(equalTo: topAnchor).isActive = true
        }
    }
}
