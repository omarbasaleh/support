//
//  UIView+pinEdges.swift
//  Code Migration
//
//  Created by Omar Basaleh on 10/21/19.
//  Copyright © 2019 Omar Basaleh. All rights reserved.
//

import Foundation

public extension UIView {
    func pinEdges(to other: UIView) {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        leadingAnchor.constraint(equalTo: other.leadingAnchor).isActive = true
        trailingAnchor.constraint(equalTo: other.trailingAnchor).isActive = true
        topAnchor.constraint(equalTo: other.topAnchor).isActive = true
        bottomAnchor.constraint(equalTo: other.bottomAnchor).isActive = true
    }
    
    func center(in other: UIView) {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        centerYAnchor.constraint(equalTo: other.centerYAnchor).isActive = true
        centerXAnchor.constraint(equalTo: other.centerXAnchor).isActive = true
    }
}
