//
//  Bundle+Support.swift
//  Nabd_Support
//
//  Created by Omar Basaleh on 1/14/20.
//  Copyright © 2020 Omar Basaleh. All rights reserved.
//

import Foundation

public extension Bundle {
    static var support: Bundle? {
        return Bundle(identifier: "com.waveline.nabd.support")
    }
}
