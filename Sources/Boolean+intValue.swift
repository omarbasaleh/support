//
//  Boolean+intValue.swift
//  Nabd_Support
//
//  Created by Omar Basaleh on 7/16/20.
//  Copyright © 2020 Omar Basaleh. All rights reserved.
//

import Foundation

public extension Bool {
    var intValue: Int {
        if self {
            return 1
        }else{
            return 0
        }
    }
}
